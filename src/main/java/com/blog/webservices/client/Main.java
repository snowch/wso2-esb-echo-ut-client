package com.blog.webservices.client;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.neethi.Policy;
import org.apache.neethi.PolicyEngine;
import org.apache.rampart.RampartMessageData;

public class Main {

	// change this to the ESB truststore location
	private static final String ESB_TRUSTSTORE_PATH = 
			"C:\\wso2\\wso2esb-4.6.0\\repository\\resources\\security\\client-truststore.jks";
	
	// change this to the policy path
	private static final String POLICY_PATH = 
			"C:\\workspace\\esb-proxy-client\\src\\main\\resources\\rampart-policy.xml";
	
	public static void main(String[] args) throws Exception {
		
		System.setProperty("javax.net.ssl.trustStore", ESB_TRUSTSTORE_PATH);
		System.setProperty("javax.net.ssl.trustStorePassword", "wso2carbon");
		System.setProperty("javax.net.ssl.trustStoreType","JKS");

		ServiceClient client = new ServiceClient(null, null);
		Options options = new Options();
		options.setAction("urn:echoString");
		options.setTo(new EndpointReference("https://localhost:8243/services/echo"));
		options.setProperty(
				RampartMessageData.KEY_RAMPART_POLICY,  
				loadPolicy(POLICY_PATH)
				);

		client.setOptions(options);
		client.engageModule("rampart");

		OMElement response = client.sendReceive(getPayload("Hello world"));
		System.out.println(response);
	}

	private static Policy loadPolicy(String xmlPath) throws Exception {
		StAXOMBuilder builder = new StAXOMBuilder(xmlPath);
		return PolicyEngine.getPolicy(builder.getDocumentElement());
	}

	private static OMElement getPayload(String value) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = factory.createOMNamespace("http://echo.services.core.carbon.wso2.org","ns1");
		OMElement elem = factory.createOMElement("echoString", ns);
		OMElement childElem = factory.createOMElement("in", null);
		childElem.setText(value);
		elem.addChild(childElem);

		return elem;
	}
}
