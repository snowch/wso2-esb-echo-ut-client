
/**
 * EchoCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Jun 25, 2013 (10:12:59 BST)
 */

    package com.blog.webservices.client;

    /**
     *  EchoCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class EchoCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public EchoCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public EchoCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for echoString method
            * override this method for handling normal response from echoString operation
            */
           public void receiveResultechoString(
                    com.blog.webservices.client.EchoStub.EchoStringResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from echoString operation
           */
            public void receiveErrorechoString(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for throwAxisFault method
            * override this method for handling normal response from throwAxisFault operation
            */
           public void receiveResultthrowAxisFault(
                    com.blog.webservices.client.EchoStub.ThrowAxisFaultResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from throwAxisFault operation
           */
            public void receiveErrorthrowAxisFault(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for echoStringArrays method
            * override this method for handling normal response from echoStringArrays operation
            */
           public void receiveResultechoStringArrays(
                    com.blog.webservices.client.EchoStub.EchoStringArraysResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from echoStringArrays operation
           */
            public void receiveErrorechoStringArrays(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for echoOMElement method
            * override this method for handling normal response from echoOMElement operation
            */
           public void receiveResultechoOMElement(
                    com.blog.webservices.client.EchoStub.EchoOMElementResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from echoOMElement operation
           */
            public void receiveErrorechoOMElement(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for echoInt method
            * override this method for handling normal response from echoInt operation
            */
           public void receiveResultechoInt(
                    com.blog.webservices.client.EchoStub.EchoIntResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from echoInt operation
           */
            public void receiveErrorechoInt(java.lang.Exception e) {
            }
                


    }
    